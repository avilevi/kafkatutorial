package examples.akka

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.Await
import scala.concurrent.duration._

import scala.util.Random

object PingPongApp extends App {
  val system = ActorSystem("MyActorSystem")
  private def randomString: String = Random.alphanumeric.take(5).mkString("")


  val config: Config = ConfigFactory.parseString(
    s"""
       | bootstrap.servers = "localhost:9092",
       | group.id = "$randomString"
       | enable.auto.commit = false
       | auto.offset.reset = "latest"
        """.stripMargin
  )


  val pongActor = system.actorOf(PongActor.props(config), "pongActor")
  val pingActor = system.actorOf(PingActor.props(config), "PingActor")

  Thread.sleep(5000)
  pongActor ! PongActor.Start

  //   This example app will ping pong 3 times and thereafter terminate the ActorSystem -
  system.whenTerminated.foreach(_ => System.exit(0))
}