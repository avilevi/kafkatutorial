package examples.akka


import akka.actor.{Actor, ActorLogging, PoisonPill, Props}
import cakesolutions.kafka.akka.KafkaConsumerActor.Confirm
import com.typesafe.config.Config

class PingActor(val config: Config) extends Actor
  with ActorLogging with PingPongConsumer with PingPongProducer{
  implicit val ec = context.dispatcher

  import PingActor._
  import PingPongProtocol._


  var counter = 0

  override def preStart() = {
    super.preStart()
    subscribe(topics)
  }

  def receive =  {
    case msgExtractor(consumerRecords) => ???

  }
}

object PingActor {
  def props(config: Config) = Props( new PingActor(config))
  val topics = List("ping")
}