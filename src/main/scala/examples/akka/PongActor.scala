package examples.akka


import akka.actor.{Actor, ActorLogging, PoisonPill, Props}
import cakesolutions.kafka.akka.KafkaConsumerActor.Confirm
import com.typesafe.config.Config

class PongActor(val config: Config) extends Actor
  with ActorLogging  with PingPongConsumer with PingPongProducer{
  import PingPongProtocol._
  import PongActor._

  override def preStart(): Unit = {
    super.preStart()
    subscribe(topics)
  }


  def receive = {
    case Start =>
      log.info("In PongActor - received start message - let the games begin")
      ???
  }

  def playingPingPong: Receive = {
    case msgExtractor(consumerRecords) => ???


  }
}

object PongActor {
  def props(config: Config) = Props(new PongActor(config))
  val topics = List("pong")
  case object Start

}
