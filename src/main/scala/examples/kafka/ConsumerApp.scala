package examples.kafka

import java.util.Collections.singletonList

import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}

import scala.collection.JavaConverters._

object ConsumerApp  extends App{
  val TOPIC = "local-fake-news"

  import java.util.Properties
  val id = "reporters"

  val  props = new Properties()
  props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100")
  props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
  props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
  props.put(ConsumerConfig.GROUP_ID_CONFIG, id)

  val consumer = new KafkaConsumer[String, String](props)

  consumer.subscribe(singletonList(TOPIC))

  def run() =  while(true) {
      val records = consumer.poll(100)
      for (record <- records.asScala) {
        println(s"Consumer id: $id, " + record)
      }
    consumer.commitAsync()
    }

  run()
}
