package examples.kafka

object Main extends App{
//  val TOPIC = "test1"
  val TOPIC = "fake-news"
  val producer = new SimpleProducer(TOPIC)
  val consumer1 = new SimpleConsumer(producer.topic,"reporters", "1")
  val consumer2 = new SimpleConsumer(producer.topic, "reporters", "2")
  producer.run()

  consumer2.run()
  consumer1.run()

}
