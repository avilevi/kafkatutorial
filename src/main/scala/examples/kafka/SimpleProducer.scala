package examples.kafka

import java.util.Properties

import org.apache.kafka.clients.producer._


class SimpleProducer(val topic: String) {

  val  props = new Properties()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](props)

  val records = List.tabulate(100)(i => new ProducerRecord[String, String](topic, s"hello-$i"))

  def run() = {
    records.foreach(producer.send)
    producer.close()
  }

}
