package examples.kafka

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

object SyncProducerApp extends App{
  val TOPIC = "local-fake-news"

  val  props = new Properties()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](props)

  val records = List.tabulate(10)(i => new ProducerRecord[String, String](TOPIC, s"news-$i"))

  def run() = {
    try {
      records.foreach { r =>
        val ack = producer.send(r).get()
        println(s"Producer $ack")

      }
    } catch {
      case e: Throwable =>
        e.printStackTrace()
    } finally {
      producer.close()
    }
  }
  run()

}
