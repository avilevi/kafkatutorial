package examples.kafka

import java.util.Collections.singletonList
import java.util.concurrent.Executors

import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}

import scala.collection.JavaConverters._

class SimpleConsumer(topic: String, group: String, id: String)  {

  import java.util.Properties

  val  props = new Properties()
  props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
  props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
  props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
  props.put(ConsumerConfig.GROUP_ID_CONFIG, group)

  val consumer = new KafkaConsumer[String, String](props)

  consumer.subscribe(singletonList(topic))

  //must run in separate thread since it is blocking
  def run() = {
    Executors.newSingleThreadExecutor.execute(() => {
      while (true) {
        val records = consumer.poll(100)
        for (record <- records.asScala) {
          println(s"Consumer $group-$id, " + record)
        }
        consumer.commitAsync()
      }
    })
  }
}