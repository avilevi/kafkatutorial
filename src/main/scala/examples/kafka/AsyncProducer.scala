package examples.kafka

import java.util.Properties

import org.apache.kafka.clients.producer._

import scala.concurrent.Promise

object AsyncProducer  extends App{
  val TOPIC = "local-fake-news"

  val  props = new Properties()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](props)

  val records = List.tabulate(10)(i => new ProducerRecord[String, String](TOPIC, s"news-$i"))

  def sendAsync(record : ProducerRecord[String, String]):Unit = {
    def cb = new Callback {
      override def onCompletion(metadata: RecordMetadata, exception: Exception): Unit = {
        if(exception != null){
          exception.printStackTrace
        }
      }
    }
    producer.send(record, cb)
  }

  def run() = {
    records.foreach(sendAsync)
    producer.close()
  }

    run()

  }
